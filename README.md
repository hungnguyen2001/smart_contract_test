#contracts folder - hold contracts defination file using "solidity" language.

#scripts folder - hold the deploy file, which help to deploy the storage contract using "web3" and "ethers" libraries

#test folder - hold the test file, which help us to test the contract before deploy

#hardhat.config.js file - which configure the network


-- Step --
#Compile contracts
$npx hardhat compile

#Testing contracts
$npx hardhat test

#Deploying contracts
$npx hardhat run --network <your-network> <your-deploy-file-path>

#Verifying contracts
$npx hardhat verify --network <your-network> <address> <unlock time>

