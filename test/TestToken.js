const { expect } = require("chai");
const hre = require("hardhat");
const { loadFixture } = require("@nomicfoundation/hardhat-network-helpers");

describe("Token Contract", function () {
    async function deployTokenFixture(){

        //get contract factory
        const Token = await hre.ethers.getContractFactory("Aidea");

        //get address signed the contract
        const [owner, address1, address2] = await hre.ethers.getSigners();

        //deploy contract 
        const hardhatToken = await Token.deploy();

        await hardhatToken.deployed();

        return {Token, hardhatToken, owner, address1, address2};
    }

    it("Get balance of owner balance", async function () {
        const { Token, hardhatToken, owner, address1, address2 } = await loadFixture(deployTokenFixture);
        const ownerBalance = await hardhatToken.balanceOf(owner.address);
        // assert that the value is correct
        expect(await hardhatToken.totalSupply()).to.equal(ownerBalance);
      });
});