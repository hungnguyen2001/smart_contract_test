pragma solidity <=0.8.18;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/access/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "hardhat/console.sol";
contract Aidea is
    ERC20("Aidea", "AID"),
    ERC20Burnable,
    Ownable
{
    //init capacity
    uint256 private cap = 50_000_000_000 * 10**uint256(18);
    constructor() {
        console.log("owner: %s maxcap: %s", msg.sender, cap);

        _mint(msg.sender, cap);
        transferOwnership(msg.sender);
    }

    //mint capacity to the owner wallet address
    function mint(address to, uint256 amount) public onlyOwner {
        require(
            ERC20.totalSupply() + amount <= cap,
            "Aidea: cap exceeded"
        );
        _mint(to, amount);
    }
}