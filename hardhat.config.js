require("@nomicfoundation/hardhat-toolbox");
require('dotenv').config();
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.9",
  networks:{
    //network
    bsctest: {
      //rpc url
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      //private key of owner wallet address
      accounts: [process.env.PRIVATE_KEY]
    },
  },
  etherscan:{
    //network api key
    apiKey: process.env.API_KEY
  }
};
